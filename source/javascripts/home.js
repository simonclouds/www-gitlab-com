(function($) {
  'use strict';

  var StickyNav = {
    placeholder: null,
    nav: null,
    $body: [],
    is_sticky: false,
    init: function() {
      this.$body = $('body');
      if ( this.$body.is('.static-nav') ) {
        this.setup();
      }
    },
    setup: function() {
      var $nav = $('.navbar-fixed-top:first');
      var $placeholder = $('<div class="sticky-nav-placeholder"/>').height($nav.innerHeight());
      this.nav = $nav.get(0);
      this.placeholder = $placeholder.get(0);
      $nav.before($placeholder);
      this.is_sticky = false;
      this.scrollCallback();
      window.an_OnScroll.addFunction(this.scrollCallback.bind(this));
    },
    scrollCallback: function() {
      var boundingRect;
      if ( this.is_sticky ) {
        // Check placeholder position
        boundingRect = this.placeholder.getBoundingClientRect();
      } else {
        // Check nav position
        boundingRect = this.nav.getBoundingClientRect();
      }

      if ( boundingRect.top <= -40 ) {
        if ( !this.is_sticky ) {
          this.$body.removeClass('static-nav');
          this.is_sticky = true;
        }
      } else if ( this.is_sticky ) {
        this.$body.addClass('static-nav');
        this.is_sticky = false;
      }
    }
  };
  $(StickyNav.init.bind(StickyNav));

  var ValuesPillars = {
    $component: [],
    $nav: [],
    $values: [],
    init: function() {
      this.$component = $('.js-values-pillars');
      if ( this.$component.length > 0 ) {
        this.setup();
      }
    },
    setup: function() {
      this.$values = this.$component.children('.value');
      $(document).on('touchstart mouseover', '.values-pillars-nav li', this.onMouseover);
      this.setupNavigation();
    },
    setupNavigation: function() {
      var $ul = $('<ul class="list-unstyled values-pillars-nav"/>');
      this.$values.first().addClass('is-active');
      this.$values.each(function() {
        var $$ = $(this);
        var $li = $('<li/>');
        $li.data('content', $$);
        $li.append( $$.find('svg:first').remove() );
        $li.append( $$.find('.sub-heading .label').remove() );
        $ul.append($li);
      });

      $ul.children().first().addClass('is-active');
      this.$component.prepend($ul);
      this.$nav = $ul;
    },
    onMouseover: function() {
      ValuesPillars.activateValue($(this));
    },
    activateValue: function($value) {
      $value
        .addClass('is-active')
        .siblings()
        .removeClass('is-active')
        .end()
        .data('content')
        .addClass('is-active')
        .siblings('.is-active')
        .removeClass('is-active')
      ;
    }
  };
  $(ValuesPillars.init.bind(ValuesPillars));

  var LifecycleDiagram = {
    _diagram: [],
    _phases: [],
    slicked: false,
    _body: [],
    _diagram_nav: [],
    init: function() {
      var self = LifecycleDiagram;
      self._diagram = $('.home-lifecycle-diagram');
      if ( self._diagram.length > 0 ) {
        self.setup();
      }
    },
    setup: function() {
      var self = this;
      self._phases = $('.lifecycle-diagram-phase');
      self._body = $('body');
      self.setupNavigation();
      self.checkHeights();
      $(document).on('mouseover', '.lifecycle-diagram-navigation li', self.onMouseover);
      $(window).on('resize', function() { self.checkHeights(); });
      $(window).on('load', function() { self.checkHeights(); });
    },
    setupNavigation: function() {
      var self = this;
      var $div = $('<div class="lifecycle-diagram-nav-wrapper"/>');
      var $ul = $('<ul class="lifecycle-diagram-navigation list-unstyled"/>');

      self._phases.each(function() {
        var $$ = $(this);
        var title = $$.data('title');
        var $icon = $$.find('svg:first').remove();
        var $iconWrapper = $('<div class="icon-wrapper"/>').append($icon);
        var $li = $('<li/>').addClass(title.toLowerCase());

        $li.data('content', $$);
        $li.append( $iconWrapper, $('<span>' + title + '</span>') );
        $ul.append($li);
      });

      $div.append('<h2 class="lifecycle-diagram-nav-title">' + self._diagram.data('nav-title') + '</h2>');
      $div.append($ul);
      self._diagram.prepend($div);
      $ul.children().first().addClass('is-active');
      self._diagram_nav = $ul;
    },
    onMouseover: function() {
      LifecycleDiagram.activatePhase($(this));
    },
    activatePhase: function($phase) {
      $phase
        .addClass('is-active')
        .siblings()
        .removeClass('is-active')
        .end()
        .data('content')
        .show()
        .siblings('.lifecycle-diagram-phase:visible')
        .hide()
      ;
    },
    checkHeights: function() {
      var self = this;
      var minHeight = 0;

      self._phases.css( 'min-height', '');
      self._diagram.addClass('is-checking-height');

      self._phases.each(function() {
        minHeight = Math.max(minHeight, $(this).height());
      });
      self._diagram.removeClass('is-checking-height');
      self._phases.css('min-height', minHeight + 'px');
    }
  };
  $(LifecycleDiagram.init);
})(jQuery);
